package com.gearsofleo.services.wallet.domain.enums;

import java.util.Arrays;

public enum TransactionTypeEnum {

    DEBIT(1L), CREDIT(2L);

    private Long id;

    TransactionTypeEnum(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public static TransactionTypeEnum getEnum(Long id) {
        return Arrays.stream(TransactionTypeEnum.values())
                .filter(type -> type.id.equals(id))
                .findFirst()
                .orElse(null);
    }

}
