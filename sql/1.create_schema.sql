CREATE TABLE wallet (
  id INT NOT NULL AUTO_INCREMENT,
  player_id varchar(100) NOT NULL,
  created_date DATETIME NOT NULL,
  last_update_date DATETIME NOT NULL,
  balance float NOT NULL,
  PRIMARY KEY (id)
);

CREATE TABLE transaction_type (
  id INT NOT NULL,
  name varchar(50) NOT NULL,
  created_date DATETIME NOT NULL,
  PRIMARY KEY (id)
);

CREATE TABLE transactions (
  id INT NOT NULL AUTO_INCREMENT,
  wallet_id INT NOT NULL,
  transaction_type_id INT NOT NULL,
  amount float NOT NULL,
  created_date DATETIME NOT NULL,
  PRIMARY KEY (id)
);


ALTER TABLE transactions ADD CONSTRAINT FK_TRANSACTION_TYPE FOREIGN KEY (transaction_type_id) REFERENCES transaction_type(id);
ALTER TABLE transactions ADD CONSTRAINT FK_WALLET_ID FOREIGN KEY (wallet_id) REFERENCES wallet(id);


CREATE UNIQUE INDEX PLAYER_ID_INDEX ON wallet (player_id);
CREATE INDEX WALLET_ID_INDEX ON transactions (wallet_id);


INSERT INTO transaction_type(id,name,created_date) VALUES
(1,'DEBIT',now()),
(2,'CREDIT',now());

