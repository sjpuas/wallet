package com.gearsofleo.services.wallet.domain;

import com.gearsofleo.services.wallet.domain.enums.TransactionTypeEnum;

import java.io.Serializable;

public class TransactionRequest implements Serializable {

    private TransactionTypeEnum transactionTypeId;
    private Double amount;

    public TransactionTypeEnum getTransactionTypeId() {
        return transactionTypeId;
    }

    public void setTransactionTypeId(TransactionTypeEnum transactionTypeId) {
        this.transactionTypeId = transactionTypeId;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }
}
