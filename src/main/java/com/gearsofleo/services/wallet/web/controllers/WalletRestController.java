package com.gearsofleo.services.wallet.web.controllers;

import com.gearsofleo.services.wallet.domain.Transaction;
import com.gearsofleo.services.wallet.domain.TransactionRequest;
import com.gearsofleo.services.wallet.domain.Wallet;
import com.gearsofleo.services.wallet.domain.exceptions.NotFoundDataException;
import com.gearsofleo.services.wallet.domain.exceptions.NotSupportOperationException;
import com.gearsofleo.services.wallet.services.WalletService;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class WalletRestController {

    private WalletService walletService;

    public WalletRestController(WalletService walletService) {
        this.walletService = walletService;
    }

    @ApiOperation(value = "Get Wallet of a player", tags = "Wallet")
    @CrossOrigin
    @GetMapping("/players/{playerId}/wallet")
    public Wallet getWallet(@PathVariable("playerId") String playerId) throws NotFoundDataException {
        Wallet wallet = walletService.getWalletByPlayerId(playerId);
        if (wallet == null) {
            throw new NotFoundDataException("Wallet for playerId not found");
        }
        return wallet;
    }

    @ApiOperation(value = "Get Transactions of a player", tags = "Transaction")
    @CrossOrigin
    @GetMapping("/players/{playerId}/wallet/transactions")
    public List<Transaction> getTransactions(@PathVariable("playerId") String playerId) throws NotFoundDataException {
        List<Transaction> transactions = walletService.getTransactionsByPlayerId(playerId);
        if (transactions == null || transactions.isEmpty()) {
            throw new NotFoundDataException("Transactions for playerId not found");
        }
        return transactions;
    }

    @ApiOperation(value = "Create a transaction of a player", tags = "Transaction")
    @CrossOrigin
    @PostMapping("/players/{playerId}/wallet/transactions")
    @ResponseStatus(HttpStatus.CREATED)
    public Transaction addTransactions(@PathVariable("playerId") String playerId,
                                       @RequestBody TransactionRequest transactionRequest) throws NotFoundDataException, NotSupportOperationException {
        Transaction transaction = walletService.addTransactionsByPlayerId(playerId, transactionRequest);
        return transaction;
    }


}
