package com.gearsofleo.services.wallet.services.transactions;

import com.gearsofleo.services.wallet.domain.enums.TransactionTypeEnum;
import com.gearsofleo.services.wallet.services.transactions.impl.CreditTransactionService;
import com.gearsofleo.services.wallet.services.transactions.impl.DebitTransactionService;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Service
public class TransactionServiceFactory {

    private Map<TransactionTypeEnum, ITransactionService> serviceMap;

    public TransactionServiceFactory(DebitTransactionService debitTransactionService, CreditTransactionService creditTransactionService) {
        serviceMap = new HashMap<>();
        serviceMap.put(TransactionTypeEnum.DEBIT, debitTransactionService);
        serviceMap.put(TransactionTypeEnum.CREDIT, creditTransactionService);
    }

    public ITransactionService getService(TransactionTypeEnum transactionType) {
        ITransactionService service = serviceMap.get(transactionType);
        if (service == null) {
            throw new RuntimeException("Service not implemented");
        }
        return service;
    }

}
