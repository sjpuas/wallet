package com.gearsofleo.services.wallet.web.error;

import com.gearsofleo.services.wallet.domain.error.ErrorResponse;
import com.gearsofleo.services.wallet.domain.exceptions.BadRequestException;
import com.gearsofleo.services.wallet.domain.exceptions.NotFoundDataException;
import com.gearsofleo.services.wallet.domain.exceptions.NotSupportOperationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.HttpMediaTypeNotSupportedException;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.ServletRequestBindingException;
import org.springframework.web.bind.UnsatisfiedServletRequestParameterException;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;

@ControllerAdvice
public class RestExceptionHandler {

    private static final Logger logger = LoggerFactory.getLogger(RestExceptionHandler.class);

    @ExceptionHandler({Exception.class})
    @RequestMapping(produces = {MediaType.APPLICATION_JSON_UTF8_VALUE})
    @ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
    public
    @ResponseBody
    ErrorResponse handleUncaughtException(Exception ex) throws IOException {
        logger.error("Internal Error", ex);
        return new ErrorResponse(HttpStatus.INTERNAL_SERVER_ERROR.value(), "internal_error", "Internal error occurred");
    }


    @RequestMapping(produces = {MediaType.APPLICATION_JSON_UTF8_VALUE})
    @ExceptionHandler({MissingServletRequestParameterException.class,
            UnsatisfiedServletRequestParameterException.class,
            ServletRequestBindingException.class
    })
    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    public
    @ResponseBody
    ErrorResponse handleRequestException(Exception ex) {
        logger.error("Bad Request", ex);
        return new ErrorResponse(HttpStatus.BAD_REQUEST.value(), "bad_request", "Error making the request");
    }


    @RequestMapping(produces = {MediaType.APPLICATION_JSON_UTF8_VALUE})
    @ExceptionHandler({HttpRequestMethodNotSupportedException.class})
    @ResponseStatus(value = HttpStatus.METHOD_NOT_ALLOWED)
    public
    @ResponseBody
    ErrorResponse handleMethodNotSupported(Exception ex) {
        return new ErrorResponse(HttpStatus.METHOD_NOT_ALLOWED.value(), "method_not_allowed", "Unsupported method");
    }

    @RequestMapping(produces = {MediaType.APPLICATION_JSON_UTF8_VALUE})
    @ExceptionHandler(HttpMediaTypeNotSupportedException.class)
    @ResponseStatus(value = HttpStatus.UNSUPPORTED_MEDIA_TYPE)
    public
    @ResponseBody
    ErrorResponse handleUnsupportedMediaTypeException(HttpMediaTypeNotSupportedException ex) throws IOException {
        return new ErrorResponse(HttpStatus.UNSUPPORTED_MEDIA_TYPE.value(), "unsupported_media_type", "Unsupported Content-type");
    }

    @RequestMapping(produces = {MediaType.APPLICATION_JSON_UTF8_VALUE})
    @ExceptionHandler(NotFoundDataException.class)
    @ResponseStatus(value = HttpStatus.NOT_FOUND)
    public
    @ResponseBody
    ErrorResponse handleUnsupportedMediaTypeException(NotFoundDataException ex) throws IOException {
        return new ErrorResponse(HttpStatus.NOT_FOUND.value(), "not_found", ex.getMessage());
    }


    @RequestMapping(produces = {MediaType.APPLICATION_JSON_UTF8_VALUE})
    @ExceptionHandler(BadRequestException.class)
    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    public
    @ResponseBody
    ErrorResponse handleBadRequestException(BadRequestException ex) throws IOException {
        return new ErrorResponse(HttpStatus.BAD_REQUEST.value(), "bad_request", ex.getMessage());
    }

    @RequestMapping(produces = {MediaType.APPLICATION_JSON_UTF8_VALUE})
    @ExceptionHandler(NotSupportOperationException.class)
    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    public
    @ResponseBody
    ErrorResponse handleNotSupportOperationException(NotSupportOperationException ex) throws IOException {
        return new ErrorResponse(HttpStatus.BAD_REQUEST.value(), "bad_request", ex.getMessage());
    }


}
