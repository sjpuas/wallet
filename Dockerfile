FROM maven:3.5-jdk-8-alpine
WORKDIR /app
COPY ./pom.xml /app/
RUN mvn dependency:resolve
COPY . /app
RUN mvn install

FROM fabric8/java-alpine-openjdk8-jre:1.4.0

RUN mkdir -p /opt/app && \
    addgroup gearsofleo  && \
    adduser -D -G gearsofleo -h /opt/app -s /bin/false gearsofleo

WORKDIR /opt/app

ENV SPRING_OUTPUT_ANSI_ENABLED=ALWAYS \
    JAVA_DEBUG= \
    JAVA_DEBUG_PORT=8787 \
    AB_OFF=true \
    JAVA_APP_JAR=app.jar \
    JAVA_APP_DIR=/opt/app \
    JAVA_OPTIONS="-Djava.security.egd=file:/dev/./urandom"

COPY --from=0 /app/target/*.jar app.jar

VOLUME /tmp

EXPOSE 9000 8787

USER gearsofleo
