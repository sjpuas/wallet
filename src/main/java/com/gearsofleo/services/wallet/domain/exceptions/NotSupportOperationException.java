package com.gearsofleo.services.wallet.domain.exceptions;

public class NotSupportOperationException extends Exception {

    public NotSupportOperationException(String message) {
        super(message);
    }
}
