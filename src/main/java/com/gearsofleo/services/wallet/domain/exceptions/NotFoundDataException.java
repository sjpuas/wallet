package com.gearsofleo.services.wallet.domain.exceptions;

public class NotFoundDataException extends Exception {

    public NotFoundDataException(String message) {
        super(message);
    }
}
