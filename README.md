# Wallet Service
 
## Requirements

For building and running the application you need:

- [Docker](https://www.docker.com)
- [Docker-Compose](https://docs.docker.com/compose/)

Or you can  use  with  

## Running the application locally with Docker

```shell
docker-compose build
docker-compose up -d
```

You can see the swagger definition in [http://localhost:9000/swagger-ui.html](http://localhost:9000/swagger-ui.html)

The microservice does´t implement security or cache, i think that we can  implement that in another layer, like with a ServiceMesh or an ApiGateway

## Database Model

![Image of Yaktocat](./database_model.png)

## API style guide

I used as Rest style guide, the following guides:
  * https://github.com/paypal/api-standards/blob/master/api-style-guide.md
  * https://cloud.google.com/apis/design/
  