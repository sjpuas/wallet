package com.gearsofleo.services.wallet.services.transactions;

import com.gearsofleo.services.wallet.domain.Transaction;
import com.gearsofleo.services.wallet.domain.TransactionRequest;
import com.gearsofleo.services.wallet.domain.Wallet;
import com.gearsofleo.services.wallet.domain.exceptions.NotSupportOperationException;

public interface ITransactionService {

    Transaction addTransactionsByPlayerId(Wallet wallet, TransactionRequest transaction) throws NotSupportOperationException;
}
