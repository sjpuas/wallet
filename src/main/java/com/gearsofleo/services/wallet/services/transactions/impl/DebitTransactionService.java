package com.gearsofleo.services.wallet.services.transactions.impl;

import com.gearsofleo.services.wallet.domain.Transaction;
import com.gearsofleo.services.wallet.domain.TransactionRequest;
import com.gearsofleo.services.wallet.domain.Wallet;
import com.gearsofleo.services.wallet.domain.enums.TransactionTypeEnum;
import com.gearsofleo.services.wallet.domain.exceptions.NotSupportOperationException;
import com.gearsofleo.services.wallet.repository.WalletRepository;
import com.gearsofleo.services.wallet.services.transactions.ITransactionService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service
public class DebitTransactionService implements ITransactionService {

    private final static Logger logger = LoggerFactory.getLogger(DebitTransactionService.class);

    private WalletRepository walletRepository;

    public DebitTransactionService(WalletRepository walletRepository) {
        this.walletRepository = walletRepository;
    }

    public Transaction addTransactionsByPlayerId(Wallet wallet, TransactionRequest transactionRequest) throws NotSupportOperationException {
        Double newBalance = wallet.getBalance() - transactionRequest.getAmount();
        if (newBalance < 0) {
            throw new NotSupportOperationException("balance is insufficient");
        }

        wallet.setBalance(newBalance);
        wallet.setLastUpdateDate(new Date());

        Transaction transaction = new Transaction();

        transaction.setAmount(transactionRequest.getAmount());
        transaction.setTransactionTypeId(TransactionTypeEnum.DEBIT.getId());
        transaction.setWalletId(wallet.getId());
        transaction.setCreatedDate(new Date());

        walletRepository.saveTransaction(transaction);
        walletRepository.updateWallet(wallet);

       return transaction;
    }
}
