package com.gearsofleo.services.wallet.services;

import com.gearsofleo.services.wallet.domain.Transaction;
import com.gearsofleo.services.wallet.domain.TransactionRequest;
import com.gearsofleo.services.wallet.domain.Wallet;
import com.gearsofleo.services.wallet.domain.exceptions.NotSupportOperationException;
import com.gearsofleo.services.wallet.repository.WalletRepository;
import com.gearsofleo.services.wallet.services.transactions.TransactionServiceFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class WalletService {

    private final static Logger logger = LoggerFactory.getLogger(WalletService.class);

    private WalletRepository walletRepository;
    private TransactionServiceFactory transactionServiceFactory;

    public WalletService(WalletRepository walletRepository, TransactionServiceFactory transactionServiceFactory) {
        this.walletRepository = walletRepository;
        this.transactionServiceFactory = transactionServiceFactory;
    }

    public Wallet getWalletByPlayerId(String playerId) {
        return walletRepository.getWalletByPlayerId(playerId);
    }

    public List<Transaction> getTransactionsByPlayerId(String playerId) {
        return walletRepository.getTransactionsByPlayerId(playerId);
    }

    public Transaction addTransactionsByPlayerId(String playerId, TransactionRequest transactionRequest) throws NotSupportOperationException {
        Wallet wallet = getWalletByPlayerId(playerId);
        if (wallet == null) {
            throw new NotSupportOperationException("wallet not found");
        }
        return transactionServiceFactory
                .getService(transactionRequest.getTransactionTypeId())
                .addTransactionsByPlayerId(wallet, transactionRequest);
    }
}
