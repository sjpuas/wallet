package com.gearsofleo.services.wallet.repository;

import com.gearsofleo.services.wallet.domain.Transaction;
import com.gearsofleo.services.wallet.domain.Wallet;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface WalletRepository {

    Wallet getWalletByPlayerId(@Param("playerId") String playerId);

    List<Transaction> getTransactionsByPlayerId(@Param("playerId") String playerId);

    void updateWallet(@Param("wallet") Wallet wallet);

    void saveTransaction(@Param("transaction") Transaction transaction);

}
