package com.gearsofleo.services.wallet.utils;

public final class ApplicationConstants {

    public static final String SPRING_PROFILE_DEFAULT = "default";
    public static final String SPRING_PROFILE_REDIS = "redis";

    private ApplicationConstants() {
    }

}
